import React from "react"
import {Row, Col, Card, CardBody, CardTitle, Progress, Button} from "reactstrap"
import classnames from "classnames"
import {TabContent, TabPane, Nav, NavItem, NavLink} from "reactstrap"
import AhuJk3L1 from "./jk3/l1/ahu";
import AhuJk3L2 from "./jk3/l2/ahu";
import AhuJk3L3 from "./jk3/l3/ahu";
import DehumJk3l1 from "./jk3/l1/dehum";
import DehumJk3l2 from "./jk3/l2/dehum";
import DehumJk3l3 from "./jk3/l3/dehum";
import CposJk3Ut from "./jk3/ut/cpos"
import {Badge} from "reactstrap"
import axios from "axios";
import FuelJk2Ut from "./jk2/ut/fuel";
import WaterJk2Ut from "./jk2/ut/water";
import FuelJk3Ut from "./jk3/ut/fuel";
import WaterJk3Ut from "./jk3/ut/water";





class Mainjkthree extends React.Component {

  state = {
    active: "1" ,
    item_1:[],
    item_2:[],
    item_3:[],
    item_4:[],
    item_5:[],
    item_6:[],
    item_7:[],
    item_8:[],
    item_9:[],
    item_10:[],
    item_11:[],
    item_12:[],
    item_13:[],
  }

  toggle = tab => {
    if (this.state.active !== tab) {
      this.setState({ active: tab })
    }
  }


  componentDidMount() {
    this.getCpos();
    this.interval = setInterval(() => {
      this.getCpos();
    }, 20000);
  }


  getCpos() {
    let urlDehumL1 = `https://api-iot.dci-indonesia.com/predictive/api/dehum/jk/3/level/L1`;
    let urlBusbarL1 = `https://api-iot.dci-indonesia.com/predictive/api/ahu/jk/3/level/L1`;

    let urlDehumL2 = `https://api-iot.dci-indonesia.com/predictive/api/dehum/jk/3/level/L2`;
    let urlBusbarL2 = `https://api-iot.dci-indonesia.com/predictive/api/ahu/jk/3/level/L2`;

    let urlDehumL3 = `https://api-iot.dci-indonesia.com/predictive/api/dehum/jk/3/level/L3`;
    let urlBusbarL3 = `https://api-iot.dci-indonesia.com/predictive/api/ahu/jk/3/level/L3`;

    let urlCpos = `https://api-iot.dci-indonesia.com/predictive/api/cpos/jk/3/level/UT`;

    const requestOne = axios.get(urlDehumL1);
    const requestTwo = axios.get(urlBusbarL1);
    const requestThree = axios.get(urlDehumL2);
    const requestFour = axios.get(urlBusbarL2);
    const requestFive = axios.get(urlDehumL3);
    const requestSix = axios.get(urlBusbarL3);
    const requestSeven = axios.get(urlCpos);


    axios
        .all([requestOne,requestTwo,requestThree,requestFour,requestFive,requestSix,requestSeven])
        .then(
            axios.spread(
                (...responses) => {

                  const responseOne = responses[0];
                  const responseTwo = responses[1];
                  const responesThree = responses[2];
                  const responesFour = responses[3];
                  const responesFive = responses[4];
                  const responesSix = responses[5];
                  const responesSeven = responses[6];



                  const alertsOne = responseOne.data.row.filter(data => data.alert === true);
                  const alertsTwo = responseTwo.data.row.filter(data => data.alert === true);

                  const alertsThree = responesThree.data.row.filter(data => data.alert === true);
                  const alertsFour = responesFour.data.row.filter(data => data.alert === true);

                  const alertsFive = responesFive.data.row.filter(data => data.alert === true);
                  const alertsSix = responesSix.data.row.filter(data => data.alert === true);

                  const alertsSeven = responesSeven.data.row.filter(data => data.alert === true);

                  const dataCountL1 = alertsOne.length+alertsTwo.length;
                  const dataCountL2 = alertsThree.length+alertsFour.length;
                  const dataCountL3 = alertsFive.length+alertsSix.length;
                  const dataCountUt = alertsSeven.length;







                  this.setState({dataCountL1});
                  this.setState({dataCountL2});
                  this.setState({dataCountL3});
                  this.setState({dataCountUt});




                }))
        .catch((e) =>
        {
          urlDehumL1 = e.response;
        }) ;
  }


  componentWillUnmount() {
    clearInterval(this.interval);
  }



  render() {
    let {dataCountL1,dataCountL2,dataCountL3,dataCountUt} = this.state;
    return (

          <Card>




            <CardTitle className={dataCountL1 + dataCountL2 + dataCountL3 + dataCountUt === 1 ? ' dashboard-warning' :
                dataCountL1 + dataCountL2 + dataCountL3 + dataCountUt > 1 ? ' dashboard-danger' :
                    'dashboard-normal'}>JK-3</CardTitle>








            <CardBody className="pl5">


              <Nav tabs className="nav-justified">

                <NavItem>
                  <Badge pill color="danger" className={dataCountL1 > 0 ? 'badge-up' : 'hilang' }>
                    {dataCountL1}
                  </Badge>
                  <NavLink

                      className={classnames({
                        active: this.state.active === "1"
                      })}
                      onClick={() => {
                        this.toggle("1")
                      }}
                  >
                    L1
                  </NavLink>

                </NavItem>
                <NavItem>
                  <Badge pill color="danger" className={dataCountL2 > 0 ? 'badge-up' : 'hilang' }>
                    {dataCountL2}
                  </Badge>
                  <NavLink
                      className={classnames({
                        active: this.state.active === "2"
                      })}
                      onClick={() => {
                        this.toggle("2")
                      }}
                  >
                    L2
                  </NavLink>
                </NavItem>
                <NavItem>
                  <Badge pill color="danger" className={dataCountL3 > 0 ? 'badge-up' : 'hilang' }>
                    {dataCountL3}
                  </Badge>
                  <NavLink
                      className={classnames({
                        active: this.state.active === "3"
                      })}
                      onClick={() => {
                        this.toggle("3")
                      }}
                  >
                    L3
                  </NavLink>
                </NavItem>
                <NavItem>
                  <Badge pill color="danger" className={dataCountUt > 0 ? 'badge-up' : 'hilang' }>
                    {dataCountUt}
                  </Badge>
                  <NavLink
                      className={classnames({
                        active: this.state.active === "4"
                      })}
                      onClick={() => {
                        this.toggle("5")
                      }}
                  >
                    UT
                  </NavLink>
                </NavItem>
              </Nav>



              <TabContent activeTab={this.state.active}>
                <TabPane tabId="1">

                  <div>

                    <div>

                      <Col lg="12" md="6" sm="12" >
                        <Card className="mb-0">
                          {/*<CardTitle className="title-equipment-main">MECHANICAL</CardTitle>*/}
                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>

                          <DehumJk3l1 />



                          <AhuJk3L1 />


                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>


                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>


                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>



                          {/*<CardTitle className="title-equipment-main">ELECTRICAL</CardTitle>*/}

                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>

                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>



                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>


                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>


                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>




                        </Card>
                      </Col>



                    </div>
                  </div>
                </TabPane>
              </TabContent>

              <TabContent activeTab={this.state.active}>
                <TabPane tabId="2">

                  <div>

                    <div>

                      <Col lg="12" md="6" sm="12" >
                        <Card className="mb-0">
                          {/*<CardTitle className="title-equipment-main">MECHANICAL</CardTitle>*/}
                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>

                          <DehumJk3l2 />



                          <AhuJk3L2 />


                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>


                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>


                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>



                          {/*<CardTitle className="title-equipment-main">ELECTRICAL</CardTitle>*/}

                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>

                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>



                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>


                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>


                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>




                        </Card>
                      </Col>



                    </div>
                  </div>
                </TabPane>
              </TabContent>


              <TabContent activeTab={this.state.active}>
                <TabPane tabId="3">

                  <div>

                    <div>

                      <Col lg="12" md="6" sm="12" >
                        <Card className="mb-0">
                          {/*<CardTitle className="title-equipment-main">MECHANICAL</CardTitle>*/}
                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>

                          <DehumJk3l3 />



                          <AhuJk3L3 />


                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>


                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>


                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>



                          {/*<CardTitle className="title-equipment-main">ELECTRICAL</CardTitle>*/}

                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>

                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>



                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>


                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>


                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>




                        </Card>
                      </Col>



                    </div>
                  </div>
                </TabPane>
              </TabContent>

              <TabContent activeTab={this.state.active}>
                <TabPane tabId="5">

                  <div>

                    <div>

                      <Col lg="12" md="6" sm="12" >
                        <Card className="mb-0">
                          {/*<CardTitle className="title-equipment-main">MECHANICAL</CardTitle>*/}
                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>

                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>



                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>


                          <CposJk3Ut />


                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>


                          <WaterJk3Ut />





                          {/*<CardTitle className="title-equipment-main">ELECTRICAL</CardTitle>*/}

                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>

                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>



                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>


                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>


                          <Row>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>

                            <Col lg="2" md="6" sm="12" className="text-center pl0">
                              <p className="fonthalfrem font-mati">0 </p>
                            </Col>
                          </Row>




                        </Card>
                      </Col>



                    </div>
                  </div>
                </TabPane>
              </TabContent>


            </CardBody>
          </Card>



    )
  }
}



export default Mainjkthree
