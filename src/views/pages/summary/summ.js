import { Link } from "react-router-dom";
import React, {Component, Fragment, Suspense} from "react";
import axios from "axios";
import Spinner from "../../components/spinners/SpinnerFlex";
import UIfx from 'uifx';
import sound from './alarm-pred.mp3';
import {
    Card,
    CardBody,
    CardImg,
    CardTitle,
    Row,
    Col,
    Button,
    Progress, Badge,
} from "reactstrap";
import Marquee from "react-fast-marquee/dist";

const loader2Style = {
    position: "absolute",
    margin: "auto",
    width: "90%",
    zIndex: "1",
};

const CustomLoader = () => (
    <div style={loader2Style}>
        <Spinner />
    </div>
);
const alarmSound= new UIfx(
    sound,
    {
        volume: 0.4, // number between 0.0 ~ 1.0
        throttleMs: 60000
    }
)

class Summ extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            level: [],
            loading : true
        };
    }

    componentDidMount() {

        this.getBar();
        this.interval = setInterval(() => {
            this.getBar();
        }, 15000);

        this.interval = setInterval(() => {
            window.location.reload();
        }, 280000);
    }




    getBar() {
        axios
                .get("https://api-iot.dci-indonesia.com/predictive/api/summary/all")
                // .get("https://budokanworldkarate.com/summary.json")

            .then((response) => {
                const data = response.data.row;
                this.state = { hasError: false };
                this.setState({ data, loading:false });
            })

            .catch(function (error) {
                if (error.response) {
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                }
            })

    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }


    componentDidCatch(error, info) {
        this.setState({ hasError: true });
        alert('something error')
    }

    render() {
        const data = this.state.data;
        if (this.state.hasError) {
            // You can render any custom fallback UI
            return <h1>Error occurred!</h1>;
        }
        return (
            <Row lg="12">
                <Col col="1" className="row-levels-padding mw-8">


                    <Card>
                        <CardTitle className="dashboard-normal">
                            .
                        </CardTitle>

                        <CardBody className="pl5">
                            <Row className="row-levels">


                                <Col
                                    lg="12"
                                    md="12"
                                    sm="12"
                                    className="text-center pl0"
                                >
                                    <p className="fonthalfrem font-title-left bg-levels item-0">
                                        ITEM
                                    </p>
                                </Col>

                            </Row>

                            <Row className="row-levels">


                                <Col
                                    lg="12"
                                    md="12"
                                    sm="12"
                                    className="text-center pl0"
                                >
                                    <p className="fonthalfrem font-title-left bg-levels">
                                        SWITCH
                                    </p>
                                </Col>

                            </Row>

                            <Row className="row-levels">


                                <Col
                                    lg="12"
                                    md="12"
                                    sm="12"
                                    className="text-center pl0"
                                >
                                    <p className="fonthalfrem font-title-left bg-levels">
                                        HYDROGEN
                                    </p>
                                </Col>

                            </Row>


                            <Row className="row-levels">


                                <Col
                                    lg="12"
                                    md="12"
                                    sm="12"
                                    className="text-center pl0"
                                >
                                    <p className="fonthalfrem font-title-left bg-levels">
                                        WATER
                                    </p>
                                </Col>

                            </Row>


                            <Row className="row-levels">


                                <Col
                                    lg="12"
                                    md="12"
                                    sm="12"
                                    className="text-center pl0"
                                >
                                    <p className="fonthalfrem font-title-left bg-levels">
                                        CPOS
                                    </p>
                                </Col>

                            </Row>


                            <Row className="row-levels">


                                <Col
                                    lg="12"
                                    md="12"
                                    sm="12"
                                    className="text-center pl0"
                                >
                                    <p className="fonthalfrem font-title-left bg-levels">
                                        DEHUM
                                    </p>
                                </Col>

                            </Row>




                            <Row className="row-levels">


                                <Col
                                    lg="12"
                                    md="12"
                                    sm="12"
                                    className="text-center pl0"
                                >
                                    <p className="fonthalfrem font-title-left bg-levels">
                                        CRAC/AHU
                                    </p>
                                </Col>

                            </Row>


                            <Row className="row-levels">


                                <Col
                                    lg="12"
                                    md="12"
                                    sm="12"
                                    className="text-center pl0"
                                >
                                    <p className="fonthalfrem font-title-left bg-levels">
                                        FUEL
                                    </p>
                                </Col>

                            </Row>

                            <Row className="row-levels">


                                <Col
                                    lg="12"
                                    md="12"
                                    sm="12"
                                    className="text-center pl0"
                                >
                                    <p className="fonthalfrem font-title-left bg-levels">
                                        BUSBAR
                                    </p>
                                </Col>

                            </Row>





                            <Row className="row-levels">


                                <Col
                                    lg="12"
                                    md="12"
                                    sm="12"
                                    className="text-center pl0"
                                >
                                    <p className="fonthalfrem font-title-left bg-levels">
                                        GEN
                                    </p>
                                </Col>

                            </Row>



                            <Row className="row-levels">


                                <Col
                                    lg="12"
                                    md="12"
                                    sm="12"
                                    className="text-center pl0"
                                >
                                    <p className="fonthalfrem font-title-left bg-levels">
                                        UPS
                                    </p>
                                </Col>

                            </Row>



                        </CardBody>
                    </Card>

                </Col>



                <Col lg="11" className="row-levels-padding summary-extra">

                    {this.state.loading ?

                        <Fragment>

                            <Col lg="4" sm="12"  className="ib prl5">

                                <div className="placeholder wave">

                                    <div className="square2"></div>

                                </div>

                            </Col>

                            <Col lg="4" sm="12"  className="ib prl5">

                                <div className="placeholder wave">

                                    <div className="square2"></div>

                                </div>

                            </Col>

                            <Col lg="4" sm="12"  className="ib prl5">

                                <div className="placeholder wave">

                                    <div className="square2"></div>

                                </div>

                            </Col>

                            <Col lg="4" sm="12"  className="ib prl5">

                                <div className="placeholder wave">

                                    <div className="square2"></div>

                                </div>

                            </Col>


                        </Fragment>


                        : null}

                    {/*<Marquee gradient={false} style={{display:'block'}}>*/}
                    {data.map((item_building, building_index,item_alert) => {
                            return (



                                <Col lg="4" md="4" sm="4" className="ib prl5" key={building_index}>
                                    <Card className="last-cellx">
                                        <CardTitle className={item_alert.alerts === 1 ? 'dashboards-warning' : item_alert.alerts > 1 ? 'dashboard-danger' : 'dashboard-normal' }>
                                            {item_building.building}
                                        </CardTitle>

                                        <CardBody className="pl5">
                                            <Row className="row-levels">


                                                <Col
                                                    lg="3"
                                                    md="12"
                                                    sm="12"
                                                    className="text-center pl0"
                                                >
                                                    <p className="fonthalfrem font-title-left bg-levels">
                                                        L1
                                                    </p>
                                                </Col>

                                                <Col
                                                    lg="3"
                                                    md="12"
                                                    sm="12"
                                                    className="text-center pl0"
                                                >
                                                    <p className="fonthalfrem font-title-left bg-levels">
                                                        L2
                                                    </p>
                                                </Col>

                                                <Col
                                                    lg="3"
                                                    md="12"
                                                    sm="12"
                                                    className="text-center pl0"
                                                >
                                                    <p className="fonthalfrem font-title-left bg-levels">
                                                        L{item_building.building==="JK-3" ? '4':'3'}
                                                    </p>
                                                </Col>

                                                <Col
                                                    lg="3"
                                                    md="12"
                                                    sm="12"
                                                    className="text-center pl0"
                                                >
                                                    <p className="fonthalfrem font-title-left bg-levels">
                                                        UT
                                                    </p>
                                                </Col>



                                            </Row>






                                            <Row className="row-levels row-levels-padding">
                                                {item_building.levels.map((item_level) => {
                                                    return (
                                                        <Col lg={3} className="row-levels row-levels-padding">
                                                            {item_level.groups.map(
                                                                (item_group, group_index) => {
                                                                    return (
                                                                        <React.Fragment>
                                                                            <div className={(item_group.variables.length > 0 ) ? (item_group.variables[0].alert === true) ? alarmSound.play() + 'hilang' : '' : '' }></div>
                                                                            <Col
                                                                                lg="12"
                                                                                md="12"
                                                                                sm="12"
                                                                                className="text-center pl0 last-cellx"
                                                                            >
                                                                                <Link to={ (item_group.variables.length > 0 ) ? item_group.variables[0].url : '#'}>

                                                                                    <div className={
                                                                                        (item_group.variables.length > 0 ) ? (item_group.variables[0].alert) ? ' font-alert fonthalfrem ' : ' font-normal fonthalfrem ' : 'fonthalfrem font-mati ' &&  item_group.status === 0 ? 'fonthalfrem white' : 'fonthalfrem font-mati'   }>
                                                                                        <Badge pill color="danger" className={ (item_group.variables.length > 0 ) ? (item_group.variables[0].alert === true) ?' badge-up badge-counter ': ' hilang' : 'hilang '}>
                                                                                            {(item_group.variables.length > 0 ) ? item_group.variables[0].count : ''}
                                                                                        </Badge>
                                                                                        <p className='dalem'>
                                                                                            { (item_group.variables.length > 0 ) ? item_group.variables[0].variableName : '' }
                                                                                        </p>
                                                                                    </div>

                                                                                </Link>
                                                                            </Col>
                                                                        </React.Fragment>
                                                                    )
                                                                }
                                                            )}
                                                        </Col>
                                                    )
                                                })}
                                            </Row>
                                        </CardBody>
                                    </Card>
                                </Col>
                            )
                        }

                    )}
                    {/*</Marquee>*/}

                </Col>
                <Col col="1" className="row-levels-padding mw-8 item-0">
                    <p>sisikiri</p>
                </Col>
                <Col lg='10' md='10' className='pr-pl-0 mt-15'>
                    <Col lg='2' className='ib pr-pl-0'>
                        <Col lg="3" md="1" sm="1" className="text-center pl0 ib">
                            <p className="fonthalfrem font-mati font-matix green">0 </p>
                        </Col>
                        <p className="fonthalfrem bg-levels-white ib legendx ">
                            : ACTIVE
                        </p>
                    </Col>

                    <Col lg='2' className='ib pr-pl-0'>
                        <Col lg="3" md="1" sm="1" className="text-center pl0 ib">
                            <p className="fonthalfrem font-mati font-matix red">0 </p>
                        </Col>
                        <p className="fonthalfrem bg-levels-white ib legendx">
                            : ALARM
                        </p>
                    </Col>

                    <Col lg='2' className='ib pr-pl-0'>
                        <Col lg="3" md="1" sm="1" className="text-center pl0 ib">
                            <p className="fonthalfrem font-mati font-matix ">0 </p>
                        </Col>
                        <p className="fonthalfrem bg-levels-white ib legendx">
                            : COMING SOON
                        </p>
                    </Col>

                    <Col lg='2' className='ib pr-pl-0'>
                        <Col lg="3" md="1" sm="1" className="text-center pl0 ib">
                            <p className="fonthalfrem font-mati white font-matix ">0 </p>
                        </Col>
                        <p className="fonthalfrem bg-levels-white ib legendx ">
                            : NOT APPLICABLE
                        </p>
                    </Col>
                </Col>
            </Row>
        );
    }
}

const color = "success ";
const statuscomp = "comphidup";
const blinkNotif = "blink-baik";
// const UserList = (props) => (
//
//
//
// )

export default Summ;
