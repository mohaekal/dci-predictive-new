import React from "react"
import {Card, CardBody, Row, Col, CardTitle, Button} from "reactstrap"
import {Badge} from "reactstrap"
import axios from "axios"
import {Link} from "react-router-dom";

class FuelJk1Ut extends React.Component {
  state = {
    item_1:[],
    item_2:[],
    item_3:[],
    item_4:[],
    item_5:[],
    item_6:[],
    dataCount:[],


  };


  componentDidMount() {
    this.getCpos();
    this.interval = setInterval(() => {
      this.getCpos();
    }, 20000);
  }


  getCpos() {
    let url = `https://api-iot.dci-indonesia.com/predictive/api/fuel/jk/1/level/UT
`;
    axios
      .get(url)
      .then(response => {
        const item_1 = response.data.row[0];
        const item_2 = response.data.row[1];
        // const item_3 = response.data.row[2];
        // const item_4 = response.data.row[3];
        // const item_5 = response.data.row[5];
        // const item_6 = response.data.row[6];






        this.setState({item_1});
        this.setState({item_2});
        // this.setState({item_3});
        // this.setState({item_4});
        // this.setState({item_5});
        // this.setState({item_6});





      })
      .catch((e) =>
      {
        url = e.response;
      });
  }


  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    let {item_1,item_2,item_3,item_4,item_5,item_6,countx,dataCount} = this.state;
    return (
      <Row>
        <Col lg="2" md="6" sm="12" className="text-center pl0" >
<div className="fonthalfrem font-title-left">
          <p className="">FUEL</p>
</div>
        </Col>
        <Col lg="2" md="6" sm="12" className="text-center pl0">
          <Link to={item_1.url}>
          <div className={item_1.alert === true ? ' font-alert fonthalfrem ' : 'font-normal fonthalfrem '}>
            <Badge pill color="danger" className={item_1.alert === true ? ' badge-up badge-counter ' : 'hilang '}>
              {item_1.count}
            </Badge>
          <p className="dalem  ">{item_1.variableName}</p>
          </div>
          </Link>
        </Col>

        <Col lg="2" md="6" sm="12" className="text-center pl0">
          <Link to={item_2.url}>
          <div className={item_2.alert === true ? ' font-alert fonthalfrem ' : 'font-normal fonthalfrem '}>
            <Badge pill color="danger" className={item_2.alert === true ? ' badge-up badge-counter ' : 'hilang '}>
              {item_2.count}
            </Badge>
            <p className="dalem  ">{item_2.variableName}</p>
          </div>
          </Link>
        </Col>

        <Col lg="2" md="6" sm="12" className="text-center pl0">
          <p className="fonthalfrem font-normal">{item_2.variableName}</p>
        </Col>

        <Col lg="2" md="6" sm="12" className="text-center pl0">
          <p className="fonthalfrem font-normal">{item_2.variableName}</p>
        </Col>

        <Col lg="2" md="6" sm="12" className="text-center pl0">
          <p className="fonthalfrem font-normal">{item_2.variableName}</p>
        </Col>
      </Row>
    )
  }
}

export default FuelJk1Ut
