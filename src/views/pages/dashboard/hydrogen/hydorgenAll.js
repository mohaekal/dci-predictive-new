import React, {Component, Fragment, useEffect, useState} from "react"
import { Link } from "react-router-dom"
import DataTable from "react-data-table-component"
import Spinner from "../../../components/spinners/SpinnerFlex"
import { color } from 'd3-color';
import { interpolateRgb } from 'd3-interpolate';
import ReactDOM from 'react-dom';
import LiquidFillGauge from 'react-liquid-gauge';
import {
    Card, CardBody,
    CardFooter, CardTitle, Col, Row
} from "reactstrap"
import axios from "axios";
import Marquee from "react-fast-marquee";
import {AlertTriangle, Clock, PhoneCall} from 'react-feather'
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faBuilding,
    faThermometer,
    faTachometerAlt,
    faWater
} from "@fortawesome/free-solid-svg-icons";
import GaugeChart from 'react-gauge-chart';
import ReactSpeedometer from "react-d3-speedometer"


class OutdoorTempAll extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            loading:true
        };
    }



    async componentDidMount() {
        try {
        const response  = await axios.get("https://api-iot.dci-indonesia.com/predictive/api/all-sensor/by-group?groupName=Hydrogen");
        const data = await response.data.row;
        this.setState({ data, loading : false })
        } catch (e) {
            alert(e)

        }
    }


    componentWillUnmount() {
        clearInterval(this.interval);
    }




    render() {


        return (

            <Row>

                {this.state.loading ?

                    <Fragment>

                        <Col lg="3" sm="12"  className="">

                            <div className="placeholder wave">

                                <div className="square"></div>

                            </div>

                        </Col>

                        <Col lg="3" sm="12"  className="">

                            <div className="placeholder wave">

                                <div className="square"></div>

                            </div>

                        </Col>

                        <Col lg="3" sm="12"  className="">

                            <div className="placeholder wave">

                                <div className="square"></div>

                            </div>

                        </Col>

                        <Col lg="3" sm="12"  className="">

                            <div className="placeholder wave">

                                <div className="square"></div>

                            </div>

                        </Col>

                        <Col lg="3" sm="12"  className="">

                            <div className="placeholder wave">

                                <div className="square"></div>

                            </div>

                        </Col>

                        <Col lg="3" sm="12"  className="">

                            <div className="placeholder wave">

                                <div className="square"></div>

                            </div>

                        </Col>

                        <Col lg="3" sm="12"  className="">

                            <div className="placeholder wave">

                                <div className="square"></div>

                            </div>

                        </Col>

                        <Col lg="3" sm="12"  className="">

                            <div className="placeholder wave">

                                <div className="square"></div>

                            </div>

                        </Col>
                    </Fragment>


                    : null}
                {this.state.data.map((item, index,props) => <UserList key={props.value} {...item} />)}


            </Row>

        );
    }

}

const textColor = '#AAA'
const UserList = (props,error) => (
    <Col lg="3" sm="12"  className="zero-seven-rem">
        <Card className="last-cellx">
            <p className={props.value > props.threshold ? "temperatureOutdoorAlert" : "temperatureOutdoor" }><FontAwesomeIcon icon={faThermometer} className="primary f-s-13"  /> Hydrogen</p>
            <p className="buildingtank"><FontAwesomeIcon icon={faBuilding} className="primary f-s-13"  /> {props.building}-{props.shortLevelName}</p>
            <CardBody>


                <CardTitle className="head-titlex"> {props.variableName}</CardTitle>

                {/*<GaugeChart id="gauge-chart5"*/}
                {/*            nrOfLevels={2}*/}
                {/*            arcsLength={'0.1111111111'}*/}
                {/*           colors={["#207ab9"]}*/}
                {/*     // colors={props.value>=30?'{["#FF5F6D", "#FFC371"]}':'["#5BE12C"]'}*/}
                {/*            percent={props.value}*/}
                {/*            arcPadding={0.02}*/}
                {/*            textColor={'#4d4d4d'}*/}
                {/*            needleColor={props.value>props.threshold?'#ff2200':'#05afef78'}*/}
                {/*            formatTextValue={value => value+' ppm'}*/}
                {/*            marginInPercent={0.03}*/}
                {/*/>*/}

                <ReactSpeedometer
                    ringWidth={20}
                    needleHeightRatio={0.7}
                    width={250}
                    height={180}
                    maxValue={props.maxValue}
                    value={props.value}
                    needleColor="red"
                    startColor="green"
                    paddingVertical={1}
                    paddingHorizontal={1}
                    segments={1}
                    endColor="blue"
                    textColor="grey"
                    textColor={textColor}
                />

                <p className="last-time" > <Clock size={11} style={{paddingBottom:1}}></Clock> {moment(props.convertedDatetime).calendar()} </p>
                <p className="max-tank" > <AlertTriangle size={11} style={{paddingBottom:1}}></AlertTriangle> {props.threshold} ppm </p>
            </CardBody>
        </Card>


    </Col>

)


export default OutdoorTempAll
