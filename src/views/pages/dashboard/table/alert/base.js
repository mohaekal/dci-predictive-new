import React from "react"
import { Row, Col } from "reactstrap"
import Breadcrumbs from "../../../../../@core/components/breadcrumbs";
import Root from "./root"



let $primary = "#7367F0",
    $danger = "#EA5455",
    $warning = "#FF9F43",
    $info = "#00cfe8",
    $primary_light = "#9c8cfc",
    $warning_light = "#FFC085",
    $danger_light = "#f29292",
    $info_light = "#1edec5",
    $stroke_color = "#e8e8e8",
    $label_color = "#e7eef7",
    $white = "#fff"

class CracTable extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Breadcrumbs
                    breadCrumbTitle="Alarm History"
                    breadCrumbParent="Alarm "
                />
                <Row className="match-height">

                    <Col lg="12" md="12">
                        <Root />
                    </Col>


                </Row>
            </React.Fragment>
        )
    }
}



export default CracTable