import React from "react"
import { Row, Col, Card, CardBody, CardTitle, Progress } from "reactstrap"
import BaseTank from "./baseTank"
import Breadcrumbs from "../../../../@core/components/breadcrumbs";

class Base extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Breadcrumbs
                    breadCrumbTitle="Tank"
                    breadCrumbParent="Tank"
                />
                <Row className="match-height">
                    <Col lg="12" md="12">

                        <BaseTank />

                    </Col>
                </Row>
            </React.Fragment>
        )
    }
}

export default Base
