import React, {Component} from "react"
import { Row, Col, Card, CardBody, CardTitle, Progress } from "reactstrap"
import axios from "axios";
import TableBase from "../table/tableBase";


class OutdoorTempAll extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
    }



    async componentDidMount() {
        const response = await fetch(`https://api-iot.dci-indonesia.com/predictive/api/all-sensor/by-group?groupName=Hydrogen`);
        const data = response.data.row;
        this.setState({ data })
    }


    getBar() {

        axios.get('https://api-iot.dci-indonesia.com/predictive/api/all-sensor/by-group?groupName=Hydrogen')


            .then(response => {
                const data = response.data.row;
                this.setState({ data })

            })

            .catch(function (error) {
                if (error.response) {
                    // Request made and server responded
                    console.log(error.response.data.row);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                } else if (error.request) {
                    // The request was made but no response was received
                    console.log(error.request);
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                }

            })



    }


    componentWillUnmount() {
        clearInterval(this.interval);
    }




    render() {


        return (

            <Row>


                {this.state.data.map((item, index,props) => <UserList key={props.value} {...item} />)}


            </Row>

        );
    }

}





const loadData = async () => {
try {
    const response  = await axios.get("https://api-iot.dci-indonesia.com/predictive/api/all-sensor/by-group?groupName=Hydrogen");
    const data = await response.data.row;
    console.log(data)
} catch (e) {
    alert(e);
}

    return (
        <React.Fragment>
            <Row className="match-height">
                <Col lg="12" md="12">

                    <p>tesstt</p>

                </Col>
            </Row>
        </React.Fragment>
    )


}



export default loadData
