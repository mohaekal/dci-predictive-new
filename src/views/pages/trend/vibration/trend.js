import React from "react"
import { Card, Row, Col } from "reactstrap"
import Breadcrumbs from "../../../../@core/components/breadcrumbs";
import Iframe from 'react-iframe'


let $primary = "#7367F0",
    $danger = "#EA5455",
    $warning = "#FF9F43",
    $info = "#00cfe8",
    $primary_light = "#9c8cfc",
    $warning_light = "#FFC085",
    $danger_light = "#f29292",
    $info_light = "#1edec5",
    $stroke_color = "#e8e8e8",
    $label_color = "#e7eef7",
    $white = "#fff"

class TrendOne extends React.Component {
    render() {
        return (
            <React.Fragment>

                <Col mb="0" lg="12" sm="12" className="custom-scroll">
                    <Card  >
                   <Iframe url="https://10.2.222.216:5601/goto/9adc986d891cfe652c00978ce0688d29"
                                width="100%"
                                position="relative"
                                id="myId"
                                className="myClassname"
                                height="550px"
                                frameBorder ='0'
                                scrolling="yes"
                                overflow ="hidden"
                                allow="fullscreen"
                                styles={{backgroundColor: "transparent", overflow: "hidden", height:"100%", width:"100%"}}

                        />
                    </Card>
                    </Col>

            </React.Fragment>
        )
    }
}



export default TrendOne