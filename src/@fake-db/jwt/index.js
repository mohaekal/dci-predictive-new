import mock from '../mock'
import jwt from 'jsonwebtoken'

const data = {
  users: [
    {
      id: 1,
      fullName: 'Lucas Adrian',
      username: 'lucas',
      password: '9de2d40072715c939ff0875c63a4f32c',
      avatar: require('@src/assets/images/portrait/small/avatar-s-11.jpg').default,
      email: 'lucasa@dci-indonesia.com',
      role: 'admin',
      ability: [
        {
          action: 'manage',
          subject: 'all'
        }
      ],
      extras: {
        eCommerceCartItemsCount: 5
      }
    },

    {
      id: 3,
      fullName: 'Mohamad Haekal',
      username: 'Haekal',
      password: '25d55ad283aa400af464c76d713c07ad',
      avatar: require('@src/assets/images/portrait/small/avatar-s-11.jpg').default,
      email: 'a@a.com',
      role: 'admin',
      ability: [
        {
          action: 'manage',
          subject: 'all'
        }
      ],
      extras: {
        eCommerceCartItemsCount: 5
      }
    },

    {
      id: 6,
      fullName: 'Jean Ageng Moriand',
      username: 'Jean',
      password: 'bad5dc0b53a6fd616f5860c55ae899a4',
      avatar: require('@src/assets/images/portrait/small/avatar-s-11.jpg').default,
      email: 'jean.a@dci-indonesia.com',
      role: 'admin',
      ability: [
        {
          action: 'manage',
          subject: 'all'
        }
      ],
      extras: {
        eCommerceCartItemsCount: 5
      }
    },

    {
      id: 5,
      fullName: 'Eric Gandawiguna',
      username: 'eric',
      password: 'd1d344f10884780acd5a590bcbac023f',
      avatar: require('@src/assets/images/portrait/small/avatar-s-11.jpg').default,
      email: 'eric.g@dci-indonesia.com',
      role: 'admin',
      ability: [
        {
          action: 'manage',
          subject: 'all'
        }
      ],
      extras: {
        eCommerceCartItemsCount: 5
      }
    },

    {
      id: 4,
      fullName: 'Burhanudin',
      username: 'Burhan',
      password: '81dc9bdb52d04dc20036dbd8313ed055',
      avatar: require('@src/assets/images/portrait/small/avatar-s-11.jpg').default,
      email: 'burhannudin@dci-indonesia.com',
      role: 'admin',
      ability: [
        {
          action: 'manage',
          subject: 'all'
        }
      ],
      extras: {
        eCommerceCartItemsCount: 5
      }
    },

    {
      id: 7,
      fullName: 'Edwin Kharisman Silaen',
      username: 'Edwin',
      password: '3e81e70f20425e52c43ef5608259b52a',
      avatar: require('@src/assets/images/portrait/small/avatar-s-11.jpg').default,
      email: 'edwin.k@dci-indonesia.com',
      role: 'admin',
      ability: [
        {
          action: 'manage',
          subject: 'all'
        }
      ],
      extras: {
        eCommerceCartItemsCount: 5
      }
    },

    {
      id: 8,
      fullName: 'Wiwied Ardiansyah',
      username: 'Wiwied',
      password: '942f839f9072f03a48771076c9abe8fd',
      avatar: require('@src/assets/images/portrait/small/avatar-s-11.jpg').default,
      email: 'wiwied.a@dci-indonesia.com',
      role: 'admin',
      ability: [
        {
          action: 'manage',
          subject: 'all'
        }
      ],
      extras: {
        eCommerceCartItemsCount: 5
      }
    },

    {
      id: 9,
      fullName: 'Fari Aditya Gatam',
      username: 'Fari',
      password: '7816f3a89fd6bc6099fd27377cd0a1df',
      avatar: require('@src/assets/images/portrait/small/avatar-s-11.jpg').default,
      email: 'fari.g@dci-indonesia.com',
      role: 'admin',
      ability: [
        {
          action: 'manage',
          subject: 'all'
        }
      ],
      extras: {
        eCommerceCartItemsCount: 5
      }
    },

    {
      id: 10,
      fullName: 'Rahmad Hidayat',
      username: 'Rahmad',
      password: '3788a2c081519f0674e75a701cb25652',
      avatar: require('@src/assets/images/portrait/small/avatar-s-11.jpg').default,
      email: 'rahmad.h@dci-indonesia.com',
      role: 'admin',
      ability: [
        {
          action: 'manage',
          subject: 'all'
        }
      ],
      extras: {
        eCommerceCartItemsCount: 5
      }
    },

    {
      id: 11,
      fullName: 'Ali Yafee Abdurahman Wahid',
      username: 'Ali',
      password: 'ee15184f24eb01848f57bf6878fde003',
      avatar: require('@src/assets/images/portrait/small/avatar-s-11.jpg').default,
      email: 'ali.y@dci-indonesia.com',
      role: 'admin',
      ability: [
        {
          action: 'manage',
          subject: 'all'
        }
      ],
      extras: {
        eCommerceCartItemsCount: 5
      }
    },

    {
      id: 12,
      fullName: 'Farraz Akbar',
      username: 'Farraz',
      password: '84dc3d1a59a42fe3e27a85034a9dcb7d',
      avatar: require('@src/assets/images/portrait/small/avatar-s-11.jpg').default,
      email: 'akbar.f@dci-indonesia.com',
      role: 'admin',
      ability: [
        {
          action: 'manage',
          subject: 'all'
        }
      ],
      extras: {
        eCommerceCartItemsCount: 5
      }
    },

    {
      id: 13,
      fullName: 'Akhmad Harry Susanto',
      username: 'Harry',
      password: '5578a8cec5978642a4966aaade95a44c',
      avatar: require('@src/assets/images/portrait/small/avatar-s-11.jpg').default,
      email: 'harry.s@dci-indonesia.com',
      role: 'admin',
      ability: [
        {
          action: 'manage',
          subject: 'all'
        }
      ],
      extras: {
        eCommerceCartItemsCount: 5
      }
    },

    {
      id: 14,
      fullName: 'Muhammad Yusron',
      username: 'Yusron',
      password: 'd93591bdf7860e1e4ee2fca799911215',
      avatar: require('@src/assets/images/portrait/small/avatar-s-11.jpg').default,
      email: 'yusron.m@dci-indonesia.com',
      role: 'admin',
      ability: [
        {
          action: 'manage',
          subject: 'all'
        }
      ],
      extras: {
        eCommerceCartItemsCount: 5
      }
    },


    {
      id: 15,
      fullName: 'Marco',
      username: 'Marco',
      password: '90bd3aac7f1dae7c6594d41cd6491fc6',
      avatar: require('@src/assets/images/portrait/small/avatar-s-11.jpg').default,
      email: 'marcoc@dci-indonesia.com',
      role: 'admin',
      ability: [
        {
          action: 'manage',
          subject: 'all'
        }
      ],
      extras: {
        eCommerceCartItemsCount: 5
      }
    },

    {
      id: 16,
      fullName: 'Technician',
      username: 'Technician',
      password: '1bb360e1da029b99de001878381245f4',
      avatar: require('@src/assets/images/portrait/small/avatar-s-11.jpg').default,
      email: 'locc@dci-indonesia.com',
      role: 'admin',
      ability: [
        {
          action: 'manage',
          subject: 'all'
        }
      ],
      extras: {
        eCommerceCartItemsCount: 5
      }
    },


    {
      id: 17,
      fullName: 'Ilham',
      username: 'Ilham',
      password: 'c81f2c8e2343769fdcd7176b302eb7fd',
      avatar: require('@src/assets/images/portrait/small/avatar-s-11.jpg').default,
      email: 'ilham.r@dci-indonesia.com',
      role: 'admin',
      ability: [
        {
          action: 'manage',
          subject: 'all'
        }
      ],
      extras: {
        eCommerceCartItemsCount: 5
      }
    },


    {
      id: 18,
      fullName: 'Mujiyono',
      username: 'Muji',
      password: '5f4dcc3b5aa765d61d8327deb882cf99',
      avatar: require('@src/assets/images/portrait/small/avatar-s-11.jpg').default,
      email: 'mujiyono@dci-indonesia.com',
      role: 'admin',
      ability: [
        {
          action: 'manage',
          subject: 'all'
        }
      ],
      extras: {
        eCommerceCartItemsCount: 5
      }
    },


    {
      id: 2,
      fullName: 'Jane Doe',
      username: 'janedoe',
      password: 'client',
      avatar: require('@src/assets/images/avatars/1-small.png').default,
      email: 'client@demo.com',
      role: 'client',
      ability: [
        {
          action: 'read',
          subject: 'ACL'
        },
        {
          action: 'read',
          subject: 'Auth'
        }
      ],
      extras: {
        eCommerceCartItemsCount: 5
      }
    }
  ]
}

// ! These two secrets shall be in .env file and not in any other file
const jwtConfig = {
  secret: 'dd5f3089-40c3-403d-af14-d0c228b05cb4',
  refreshTokenSecret: '7c4c1c50-3230-45bf-9eae-c9b2e401c767',
  expireTime: '10m',
  refreshTokenExpireTime: '10m'
}

mock.onPost('/jwt/login').reply(request => {
  const { email} = JSON.parse(request.data)
  const {password} = JSON.parse(request.data)
  let md5 = require('md5');

  let error = {
    email: ['Something went wrong']
  }

  const user = data.users.find(u => u.email === email && u.password === md5(password))

  if (user) {
    try {
      const accessToken = jwt.sign({ id: user.id }, jwtConfig.secret, { expiresIn: jwtConfig.expireTime })
      const refreshToken = jwt.sign({ id: user.id }, jwtConfig.refreshTokenSecret, {
        expiresIn: jwtConfig.refreshTokenExpireTime
      })

      const userData = { ...user }

      delete userData.password

      const response = {
        userData,
        accessToken,
        refreshToken
      }

      return [200, response]
    } catch (e) {
      error = e
    }
  } else {
    error = {
      email: ['Email or Password is Invalid']
    }
  }

  return [400, { error }]
})

mock.onPost('/jwt/register').reply(request => {
  if (request.data.length > 0) {
    const { email, password, username } = JSON.parse(request.data)
    const isEmailAlreadyInUse = data.users.find(user => user.email === email)
    const isUsernameAlreadyInUse = data.users.find(user => user.username === username)
    const error = {
      email: isEmailAlreadyInUse ? 'This email is already in use.' : null,
      username: isUsernameAlreadyInUse ? 'This username is already in use.' : null
    }

    if (!error.username && !error.email) {
      const userData = {
        email,
        password,
        username,
        fullName: '',
        avatar: null,
        role: 'admin',
        ability: [
          {
            action: 'manage',
            subject: 'all'
          }
        ]
      }

      // Add user id
      const length = data.users.length
      let lastIndex = 0
      if (length) {
        lastIndex = data.users[length - 1].id
      }
      userData.id = lastIndex + 1

      data.users.push(userData)

      const accessToken = jwt.sign({ id: userData.id }, jwtConfig.secret, { expiresIn: jwtConfig.expireTime })

      const user = Object.assign({}, userData)
      delete user['password']
      const response = { user, accessToken }

      return [200, response]
    } else {
      return [200, { error }]
    }
  }
})

mock.onPost('/jwt/refresh-token').reply(request => {
  const { refreshToken } = JSON.parse(request.data)

  try {
    const { id } = jwt.verify(refreshToken, jwtConfig.refreshTokenSecret)

    const userData = { ...data.users.find(user => user.id === id) }

    const newAccessToken = jwt.sign({ id: userData.id }, jwtConfig.secret, { expiresIn: jwtConfig.expiresIn })
    const newRefreshToken = jwt.sign({ id: userData.id }, jwtConfig.refreshTokenSecret, {
      expiresIn: jwtConfig.refreshTokenExpireTime
    })

    delete userData.password
    const response = {
      userData,
      accessToken: newAccessToken,
      refreshToken: newRefreshToken
    }

    return [200, response]
  } catch (e) {
    const error = 'Invalid refresh token'
    return [401, { error }]
  }
})
