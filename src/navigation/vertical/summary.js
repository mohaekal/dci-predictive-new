import { Info, Circle, BarChart,AlertTriangle } from 'react-feather'

export default [
  {
    id: 'summary',
    title: 'Summary',
    icon: <Info size={20} />,
    // badge: 'light-warning',
    //     // badgeText: '2',
    navLink: '/dashboard/summary'
    // children: [
    //   {
    //     id: 'analyticsDash',
    //     title: 'Analytics',
    //     icon: <Circle size={12} />,
    //     navLink: '/dashboard/analytics'
    //   },
    //   {
    //     id: 'eCommerceDash',
    //     title: 'eCommerce',
    //     icon: <Circle size={12} />,
    //     navLink: '/dashboard/ecommerce'
    //   }
    // ]
  },


  {
    id: 'trendlog',
    title: 'Trend Log',
    icon: <BarChart size={20} />,
    // badge: 'light-warning',
    //     // badgeText: '2',

    children: [
      {
        id: 'temperature',
        title: 'Temperatures',
        icon: <Circle size={12} />,
        navLink: '/dashboard/trend/temperature'
      },
      {
        id: 'vibration',
        title: 'Vibration',
        icon: <Circle size={12} />,
        navLink: '/dashboard/trend/vibration'
      }
    ]
  },

  {
    id: 'alert',
    title: 'Alerts',
    icon: <AlertTriangle size={20} />,
    // badge: 'light-warning',
    //     // badgeText: '2',
    navLink: '/alerts'
    // children: [
    //   {
    //     id: 'analyticsDash',
    //     title: 'Analytics',
    //     icon: <Circle size={12} />,
    //     navLink: '/dashboard/analytics'
    //   },
    //   {
    //     id: 'eCommerceDash',
    //     title: 'eCommerce',
    //     icon: <Circle size={12} />,
    //     navLink: '/dashboard/ecommerce'
    //   }
    // ]
  }

]
